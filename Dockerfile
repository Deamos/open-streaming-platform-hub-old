FROM alpine:latest
MAINTAINER David Lockwood

ARG NGINX_VERSION=1.17.3

ARG DEFAULT_DB_URL="sqlite:///db/database.db"
ARG DEFAULT_FLASK_SECRET="CHANGEME"
ARG DEFAULT_FLASK_SALT="CHANGEME"
ARG DEFAULT_TZ="ETC/UTC"

ENV DB_URL=$DEFAULT_DB_URL
ENV FLASK_SECRET=$DEFAULT_FLASK_SECRET
ENV FLASK_SALT=$DEFAULT_FLASK_SALT

EXPOSE 80/tcp
EXPOSE 443/tcp

# Get initial dependancies
RUN apk update
RUN apk add alpine-sdk \
  pcre-dev \
  libressl2.7-libcrypto \
  openssl-dev \
  libffi-dev \
  wget \
  git \
  linux-headers \
  zlib-dev

RUN apk add --no-cache tzdata

ENV TZ=$DEFAULT_TZ

RUN apk add --no-cache bash

# Make OSP Install Directory
COPY . /opt/osphub/

# Create the www-data user
RUN set -x ; \
  addgroup -g 82 -S www-data ; \
  adduser -u 82 -D -S -G www-data www-data && exit 0 ; exit 1

# Set the OSP directory to www-data
RUN chown -R www-data:www-data /opt/osphub

# Download NGINX
RUN cd /tmp && \
  wget https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && \
  tar zxf nginx-${NGINX_VERSION}.tar.gz && \
  rm nginx-${NGINX_VERSION}.tar.gz

# Compile NGINX with the NGINX-RTMP Module
RUN cd /tmp/nginx-${NGINX_VERSION} && \
  ./configure \
  --with-http_ssl_module \
  --with-http_v2_module \
  --with-cc-opt="-Wimplicit-fallthrough=0" && \
  cd /tmp/nginx-${NGINX_VERSION} && make && make install

# Configure NGINX
RUN cp /opt/osphub/setup/nginx/*.conf /usr/local/nginx/conf/
RUN cp /opt/osphub/setup/nginx/mime.types /usr/local/nginx/conf/

# Install Python, Gunicorn, and uWSGI
RUN apk add python3 \
  py3-setuptools \
  python3-dev \
  uwsgi-python3

# Install OSP Dependancies
RUN pip3 install -r /opt/osphub/setup/requirements.txt
RUN pip3 install cryptography

# Upgrade PIP
RUN pip3 install --upgrade pip

# Install Supervisor
RUN apk add supervisor
RUN mkdir -p /var/log/supervisor

VOLUME ["/usr/local/nginx/conf", "/opt/osphub/db", "/opt/osp/conf"]

RUN chmod +x /opt/osphub/setup/docker/entrypoint.sh
ENTRYPOINT ["/bin/sh","-c", "/opt/osphub/setup/docker/entrypoint.sh"]
