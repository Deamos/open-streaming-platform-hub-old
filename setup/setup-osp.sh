#!/usr/bin/env bash

cwd=$PWD

# Get Dependancies
sudo apt-get install build-essential libpcre3 libpcre3-dev libssl-dev unzip -y

# Setup Python
sudo apt-get install python3 python3-pip gunicorn3 uwsgi-plugin-python -y
sudo pip3 install -r requirements.txt

# Setup OSP Hub Directory
mkdir -p /opt/osphub
if cd ..
then
        sudo cp -rf -R * /opt/osphub
else
        echo "Unable to find installer directory. Aborting!"
        exit 1
fi

# Build Nginx
if cd /tmp
then
        sudo wget "http://nginx.org/download/nginx-1.13.10.tar.gz"
        sudo wget "http://www.zlib.net/zlib-1.2.11.tar.gz"
        sudo tar xvfz nginx-1.13.10.tar.gz
        sudo tar xvfz zlib-1.2.11.tar.gz
        if cd nginx-1.13.10
        then
                ./configure --with-http_ssl_module --with-zlib=../zlib-1.2.11
                make
                sudo make install
        else
                echo "Unable to Build Nginx! Aborting."
                exit 1
        fi
else
        echo "Unable to Download Nginx due to missing /tmp! Aborting."
        exit 1
fi

# Grab Configuration
if cd $cwd/nginx
then
        sudo cp nginx.conf /usr/local/nginx/conf/nginx.conf
else
        echo "Unable to find downloaded Nginx config directory.  Aborting."
        exit 1
fi
# Enable SystemD
if cd $cwd/nginx
then
        sudo cp nginx-osp.service /lib/systemd/system/nginx-osphub.service
        sudo systemctl daemon-reload
        sudo systemctl enable nginx-osphub.service
else
        echo "Unable to find downloaded Nginx config directory. Aborting."
        exit 1
fi

if cd $cwd/gunicorn
then
        sudo cp osp.service /lib/systemd/system/
        sudo systemctl daemon-reload
        sudo systemctl enable osphub.service
else
        echo "Unable to find downloaded Gunicorn config directory. Aborting."
        exit 1
fi


sudo chown -R www-data:www-data /opt/osphub
sudo chown -R www-data:www-data /opt/osphub/.git

# Fix for Gunicorn Logs
sudo mkdir -p /var/log/gunicorn
sudo chown -R www-data:www-data /var/log/gunicorn

# Start Nginx
sudo systemctl start nginx-osphub.service
sudo systemctl start osphub

echo "OSP Hub Install Completed! Please copy /opthub/osp/conf/config.py.dist to /opt/osphub/conf/config.py and review"
