#!/usr/bin/env bash
echo 'Placing Configuration Files'
cp -R -u -p /opt/osphub/setup/nginx/*.conf /usr/local/nginx/conf/
cp -u -p /opt/osphub/setup/nginx/mime.types /usr/local/nginx/conf/

echo 'Setting up Directories'
mkdir -p /var/log/gunicorn
chown -R www-data:www-data /var/log/gunicorn
chown -R www-data:www-data /opt/osphub
echo 'Setting up OSP Hub Configuration'

export DB_URL
echo "dbLocation='$DB_URL'" > /opt/osphub/conf/config.py
export FLASK_SECRET
echo "secretKey='$FLASK_SECRET'" >> /opt/osphub/conf/config.py
export FLASK_SALT
echo "passwordSalt='$FLASK_SALT'" >> /opt/osphub/conf/config.py

chown -R www-data:www-data /opt/osphub/conf/config.py
echo 'Performing DB Migrations'
cd /opt/osphub
python3 manage.py db init
python3 manage.py db migrate
python3 manage.py db upgrade
cd /
echo 'Starting OSP Hub'
supervisord --nodaemon --configuration /opt/osphub/setup/docker/supervisord.conf