#----------------------------------------------------------------------------#
# Imports
#----------------------------------------------------------------------------#
import sys
from os import path
import datetime
import json
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from flask import Blueprint, request, url_for
from flask_restplus import Api, Resource, reqparse
from flask_socketio import emit

from classes import servers
from classes import channels
from classes import topics
from classes import streamers
from classes import videos
from classes import clips
from classes import streams

from classes.shared import db

def str_to_bool(s):
    if s.lower() == 'true':
         return True
    elif s.lower() == 'false':
         return False
    else:
         raise ValueError # evil ValueError that doesn't tell you what the wrong value was

#----------------------------------------------------------------------------#
# Api Config.
#----------------------------------------------------------------------------#

class fixedAPI(Api):
    # Monkeyfixed API IAW https://github.com/noirbizarre/flask-restplus/issues/223
    @property
    def specs_url(self):
        '''
        The Swagger specifications absolute url (ie. `swagger.json`)

        :rtype: str
        '''
        return url_for(self.endpoint('specs'), _external=False)

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}

api_v1 = Blueprint('api', __name__, url_prefix='/apiv1')
api = fixedAPI(api_v1, version='1.0', title='OSP Hub API', description='OSP Hub API', default='Primary', default_label='OSP Hub Primary Endpoints', authorizations=authorizations)

serverAddPost = reqparse.RequestParser()
serverAddPost.add_argument('verificationToken', type=str, required=True)
serverAddPost.add_argument('serverAddress', type=str, required=True)

updatePost = reqparse.RequestParser()
updatePost.add_argument('serverToken', type=str, required=True)
updatePost.add_argument('jsonData', type=str, required=True)

keepalivePost = reqparse.RequestParser()
keepalivePost.add_argument('serverToken', type=str, required=True)
keepalivePost.add_argument('serverAddress', type=str, required=True)
#----------------------------------------------------------------------------#
# API Controllers.
#----------------------------------------------------------------------------#

@api.route('/servers')
class api_1_Server(Resource):
    # Server - Get Basic Server Information
    def get(self):
        """
            Displays a Listing of Server Settings
        """
        serverList = servers.servers.query.all()
        db.session.commit()
        return {'results': [ob.serialize() for ob in serverList]}

    @api.expect(serverAddPost)
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def post(self):
        """
            Adds a New Server to the Hub
        """
        args = serverAddPost.parse_args()
        if 'verificationToken' in args and 'serverAddress' in args:
            existingServer = servers.servers.query.filter_by(verificationToken=args['verificationToken'], siteAddress=args['serverAddress']).first()
            if existingServer != None:
                db.session.delete(existingServer)
                db.session.commit()

            newServer = servers.servers(str(args['verificationToken']), str(args['serverAddress']))
            db.session.add(newServer)
            db.session.commit()
            db.session.close()
            return {'results': {'error': 0, 'message': 'Server Added, Pending Validation', 'verificationToken': args['verificationToken']}}, 200
        db.session.close()
        return {'results': {'message': "Request Error"}}, 400

    @api.expect(serverAddPost)
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def delete(self):
        """
            Deletes a Server from the Hub
        """
        args = serverAddPost.parse_args()
        if 'verificationToken' in args and 'serverAddress' in args:
            existingServer = servers.servers.query.filter_by(verificationToken=args['verificationToken'], siteAddress=args['serverAddress']).first()
            if existingServer != None:

                existingClip = clips.clips.query.filter_by(serverID=existingServer.id).all()
                for clip in existingClip:
                    db.session.delete(clip)
                db.session.commit()

                existingStream = streams.streams.query.filter_by(serverID=existingServer.id).all()
                for stream in existingStream:
                    db.session.delete(stream)
                db.session.commit()

                existingVideo = videos.videos.query.filter_by(serverID=existingServer.id).all()
                for vid in existingVideo:
                    db.session.delete(vid)
                db.session.commit()

                existingChannel = channels.channels.query.filter_by(serverID=existingServer.id).all()
                for channel in existingChannel:
                    db.session.delete(channel)
                db.session.commit()

                existingTopic = topics.topics.query.filter_by(serverID=existingServer.id).all()
                for topic in existingTopic:
                    db.session.delete(topic)
                db.session.commit()

                existingStreamer = streamers.streamers.query.filter_by(serverID=existingServer.id).all()
                for streamer in existingStreamer:
                    db.session.delete(streamer)
                db.session.commit()

                db.session.delete(existingServer)
                db.session.commit()
                db.session.close()
                return {'results': {'error': 0, 'message': 'Server Deleted','verificationToken': args['verificationToken']}}, 200
        db.session.close()
        return {'results': {'message': "Request Error"}}, 400

@api.route('/update')
class api_1_update(Resource):
    @api.expect(updatePost)
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def post(self):
        """
            Endpoint for Nodes to provide JSON dump of Metadata to update Hub
        :return:
        """
        args = updatePost.parse_args()
        if 'serverToken' in args:
            serverQuery = servers.servers.query.filter_by(serverToken=args['serverToken']).first()
            if serverQuery != None:
                if 'jsonData' in args:
                    jsonData = None
                    try:
                        jsonData = json.loads(args['jsonData'])
                    except:
                        serverQuery.lastSeen = datetime.datetime.now()
                        serverQuery.lastStatus = 9
                        db.session.commit()
                        db.session.close()
                        return {'results': {'message': "Invalid Payload"}}, 400

                    existingClip = clips.clips.query.filter_by(serverID=serverQuery.id).all()
                    for clip in existingClip:
                        db.session.delete(clip)
                    db.session.commit()

                    existingStream = streams.streams.query.filter_by(serverID=serverQuery.id).all()
                    for stream in existingStream:
                        db.session.delete(stream)
                    db.session.commit()

                    existingVideo = videos.videos.query.filter_by(serverID=serverQuery.id).all()
                    for vid in existingVideo:
                        db.session.delete(vid)
                    db.session.commit()

                    existingChannel = channels.channels.query.filter_by(serverID=serverQuery.id).all()
                    for channel in existingChannel:
                        db.session.delete(channel)
                    db.session.commit()

                    existingTopic = topics.topics.query.filter_by(serverID=serverQuery.id).all()
                    for topic in existingTopic:
                        db.session.delete(topic)
                    db.session.commit()

                    existingStreamer = streamers.streamers.query.filter_by(serverID=serverQuery.id).all()
                    for streamer in existingStreamer:
                        db.session.delete(streamer)
                    db.session.commit()

                    topicData = jsonData['topics']
                    for topic in topicData:
                        hubID = str(serverQuery.serverUUID) + "T" + str(topic)
                        newTopic = topics.topics(hubID, serverQuery.id, topicData[topic]['name'], serverQuery.siteProtocol + serverQuery.siteAddress + "/images/" + str(topicData[topic]['img']))
                        db.session.add(newTopic)
                    db.session.commit()

                    streamerData = jsonData['streamers']
                    for streamer in streamerData:
                        hubID = str(serverQuery.serverUUID) + "S" + str(streamer)
                        newStreamer = streamers.streamers(hubID, serverQuery.id, streamerData[streamer]['username'], streamerData[streamer]['biography'], serverQuery.siteProtocol + serverQuery.siteAddress + "/images/"
                                                          + str(streamerData[streamer]['img']), serverQuery.siteProtocol + serverQuery.siteAddress + streamerData[streamer]['location'])
                        db.session.add(newStreamer)
                    db.session.commit()

                    channelData = jsonData['channels']
                    for chan in channelData:
                        hubID = str(serverQuery.serverUUID) + "C" + str(chan)
                        streamer = str(serverQuery.serverUUID) + "S" + str(channelData[chan]['streamer'])
                        topic = str(serverQuery.serverUUID) + "T" + str(channelData[chan]['topic'])
                        newChannel = channels.channels(hubID, serverQuery.id, channelData[chan]['name'], serverQuery.siteProtocol + serverQuery.siteAddress + channelData[chan]['location'], streamer, topic, channelData[chan]['protected'], int(channelData[chan]['views']),
                                                       int(channelData[chan]['currentViewers']), serverQuery.siteProtocol + serverQuery.siteAddress + "/images/" + str(channelData[chan]['img']), channelData[chan]['description'])
                        db.session.add(newChannel)
                    db.session.commit()

                    videoData = jsonData['videos']
                    for video in videoData:
                        hubID = str(serverQuery.serverUUID) + "V" + str(video)
                        streamer = str(serverQuery.serverUUID) + "S" + str(videoData[video]['streamer'])
                        topic = str(serverQuery.serverUUID) + "T" + str(videoData[video]['topic'])
                        channel = str(serverQuery.serverUUID) + "C" + str(videoData[video]['channelID'])
                        length = float(0)
                        if videoData[video]['length'] is not None:
                            length = float(videoData[video]['length'])
                        newVideo = videos.videos(hubID, serverQuery.id, videoData[video]['name'], serverQuery.siteProtocol + serverQuery.siteAddress + videoData[video]['location'], streamer, topic, channel, int(videoData[video]['views']), length,
                                                 serverQuery.siteProtocol + serverQuery.siteAddress + "/videos/" + str(videoData[video]['img']), videoData[video]['description'], int(videoData[video]['upvotes']))
                        db.session.add(newVideo)
                    db.session.commit()

                    clipData = jsonData['clips']
                    for clip in clipData:
                        hubID = str(serverQuery.serverUUID) + "P" + str(clip)
                        parentVideo = str(serverQuery.serverUUID) + "V" + str(clipData[clip]['parentVideo'])
                        length = float(0)
                        if clipData[clip]['length'] is not None:
                            length = float(clipData[clip]['length'])
                        newClip = clips.clips(hubID, serverQuery.id, clipData[clip]['name'], serverQuery.siteProtocol + serverQuery.siteAddress + clipData[clip]['location'], parentVideo, int(clipData[clip]['views']), length, serverQuery.siteProtocol + serverQuery.siteAddress + "/videos/"
                                              + str(clipData[clip]['img']), clipData[clip]['description'], int(clipData[clip]['upvotes']))
                        db.session.add(newClip)
                    db.session.commit()

                    streamData = jsonData['streams']
                    for stream in streamData:
                        hubID = str(serverQuery.serverUUID) + "M" + str(stream)
                        streamer = str(serverQuery.serverUUID) + "S" + str(streamData[stream]['streamer'])
                        topic = str(serverQuery.serverUUID) + "T" + str(streamData[stream]['topic'])
                        channel = str(serverQuery.serverUUID) + "C" + str(streamData[stream]['channelID'])
                        newStream = streams.streams(hubID, serverQuery.id, streamData[stream]['name'], serverQuery.siteProtocol + serverQuery.siteAddress + streamData[stream]['location'], streamer, topic, channel, int(streamData[stream]['views']),
                                                    int(streamData[stream]['currentViewers']), serverQuery.siteProtocol + serverQuery.siteAddress + "/stream-thumb/"
                                                    + str(streamData[stream]['img']), int(streamData[stream]['upvotes']))
                        db.session.add(newStream)
                    db.session.commit()

                    serverQuery = servers.servers.query.filter_by(serverToken=args['serverToken']).first()
                    serverQuery.lastSeen = datetime.datetime.now()
                    serverQuery.lastStatus = 1
                    db.session.commit()
                    db.session.close()
                    return {'results': {'error': 0, 'message': 'Data Received'}}, 200
                else:
                    serverQuery = servers.servers.query.filter_by(serverToken=args['serverToken']).first()
                    serverQuery.lastSeen = datetime.datetime.now()
                    serverQuery.lastStatus = 9
                    db.session.commit()
                    db.session.close()
                    return {'results': {'message': "Missing Payload"}}, 400

        db.session.close()
        return {'results': {'message': "Request Error"}}, 400

@api.route('/keepalive')
class api_1_keepalive(Resource):
    # keepalive - Handles Node Side Keepalive Pings
    @api.expect(keepalivePost)
    @api.doc(responses={200: 'Success', 400: 'Request Error'})
    def post(self):
        """
            Handles Node Keep Alive Pings
        :return:
        """
        args = keepalivePost.parse_args()
        if 'serverToken' in args and 'serverAddress' in args:
            exisitingServer = servers.servers.query.filter_by(serverAddress=args['serverAddress'], serverToken=args['serverToken']).first()
            if exisitingServer != None:
                exisitingServer.lastSeen = datetime.datetime.now()
                db.session.commit()
                db.session.close()
                return {'results': {'error': 0, 'message': 'Keepalive ACK'}}, 200
        db.session.close()
        return {'results': {'message': "Request Error"}}, 400