from .shared import db
from secrets import token_hex
from uuid import uuid4

class servers(db.Model):
    __tablename__ = "servers"
    id = db.Column(db.Integer, primary_key=True)
    serverUUID = db.Column(db.String(255), unique=True)
    siteName = db.Column(db.String(255))
    siteAddress = db.Column(db.String(255))
    siteProtocol = db.Column(db.String(32))
    siteLogo = db.Column(db.String(255))
    serverMessage = db.Column(db.String(255))
    version = db.Column(db.String(255))
    verificationToken = db.Column(db.String(2056))
    serverToken = db.Column(db.String(2056))
    validated = db.Column(db.Boolean)
    lastSeen = db.Column(db.DateTime)
    lastStatus = db.Column(db.Integer)

    topics = db.relationship('topics', backref='server', lazy="joined")
    channels = db.relationship('channels', backref='server', lazy="joined")
    streamers = db.relationship('streamers', backref='server', lazy="joined")
    videos = db.relationship('videos', backref='server', lazy="joined")
    clips = db.relationship('clips', backref='server', lazy="joined")
    streams = db.relationship('streams', backref='server', lazy="joined")


    def __init__(self, verificationToken, siteAddress):
        self.serverUUID = str(uuid4())
        self.siteName = ""
        self.siteProtocol = ""
        self.siteAddress = siteAddress
        self.siteLogo = ""
        self.serverMessage = ""
        self.version = ""
        self.verificationToken = verificationToken
        self.validated = False
        self.serverToken = None
        self.lastSeen = None
        self.lastStatus = 0  # 0: No Status, 1: Success, 9: Failure

    def __repr__(self):
        return '<id %r>' % self.id

    def generate_serverToken(self):
        self.serverToken = token_hex(256)
        return self.serverToken

    def validate(self):
        self.generate_serverToken()
        self.validated = True
        return self.serverToken

    def serialize(self):
        return {
            'siteName': self.siteName,
            'siteAddress': self.siteProtocol + self.siteAddress,
            'siteLogo': self.siteLogo,
            'serverMessage': self.serverMessage,
            'version': self.version,
            'validated': self.validated,
            'lastSeen': str(self.lastSeen),
            'lastStatus': str(self.lastStatus)
        }
